package com.ignatov.tinnews;


import android.content.Context;
import android.support.annotation.NonNull;

import com.ignatov.tinnews.dagger.application.AppComponent;
import com.ignatov.tinnews.dagger.application.AppModule;
import com.ignatov.tinnews.dagger.application.DaggerAppComponent;

public class Application extends android.app.Application {
    @NonNull
    private AppComponent appComponent;

    @NonNull
    public static Application get(@NonNull Context context) {
        return (Application) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    @NonNull
    public AppComponent applicationComponent() {
        return appComponent;
    }
}
