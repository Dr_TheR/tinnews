package com.ignatov.tinnews.utils;

import android.os.Build;
import android.support.annotation.CheckResult;
import android.text.Html;
import android.text.Spanned;

public class TextUtils {
    @CheckResult
    public boolean equals(CharSequence a, CharSequence b) {
        return android.text.TextUtils.equals(a, b);
    }

    public boolean isEmpty(CharSequence str) {
        return android.text.TextUtils.isEmpty(str);
    }

    public Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }
}
