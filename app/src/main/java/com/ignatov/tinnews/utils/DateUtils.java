package com.ignatov.tinnews.utils;

import java.util.Date;

public class DateUtils {
    /**
     * @return the value <code>0</code> if the argument Date is equal to
     * this Date; a value less than <code>0</code> if this Date
     * is before the Date argument; and a value greater than
     */
    public int compare(Date date1, Date date2) {
        if (date1 == null && date2 == null) {
            return 0;
        }
        if (date1 == null) {
            return -1;
        }
        if (date2 == null) {
            return 1;
        }
        return date1.compareTo(date2);
    }
}
