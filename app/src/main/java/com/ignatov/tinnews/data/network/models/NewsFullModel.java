package com.ignatov.tinnews.data.network.models;


import java.util.Date;

public class NewsFullModel {
    public NewsShortModel title;
    public Date creationDate;
    public Date lastModificationDate;
    public String content;
    public int bankInfoTypeId;
    public String typeId;
}
