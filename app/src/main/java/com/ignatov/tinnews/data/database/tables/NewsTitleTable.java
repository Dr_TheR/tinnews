package com.ignatov.tinnews.data.database.tables;


import android.support.annotation.NonNull;

public class NewsTitleTable {
    public static final String TABLE = "news";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TEXT = "text";
    public static final String COLUMN_PUBLICATION_DATE = "creation_date";

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE
                + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + COLUMN_NAME + " TEXT NOT NULL, "
                + COLUMN_TEXT + " TEXT NOT NULL, "
                + COLUMN_PUBLICATION_DATE + " INTEGER NOT NULL"
                + ");";
    }

    @NonNull
    public static String getDeleteTableQuery() {
        return "DROP TABLE IF EXISTS " + TABLE;
    }
}
