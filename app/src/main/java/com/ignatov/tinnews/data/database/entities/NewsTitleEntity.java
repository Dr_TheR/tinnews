package com.ignatov.tinnews.data.database.entities;

import android.support.annotation.NonNull;

import com.ignatov.tinnews.data.database.tables.NewsTitleTable;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

@SuppressWarnings("NullableProblems")
@StorIOSQLiteType(table = NewsTitleTable.TABLE)
public class NewsTitleEntity {
    @NonNull
    @StorIOSQLiteColumn(name = NewsTitleTable.COLUMN_ID, key = true)
    public Long id;

    @NonNull
    @StorIOSQLiteColumn(name = NewsTitleTable.COLUMN_NAME)
    public String name;

    @NonNull
    @StorIOSQLiteColumn(name = NewsTitleTable.COLUMN_TEXT)
    public String text;

    @NonNull
    @StorIOSQLiteColumn(name = NewsTitleTable.COLUMN_PUBLICATION_DATE)
    public Long publicationDate;

    public NewsTitleEntity() {
    }
}
