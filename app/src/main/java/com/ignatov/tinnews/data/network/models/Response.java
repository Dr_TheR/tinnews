package com.ignatov.tinnews.data.network.models;


public class Response {
    public final String resultCode;

    public Response(String resultCode) {
        this.resultCode = resultCode;
    }
}
