package com.ignatov.tinnews.data.network.models;


import java.util.List;

public class NewsResponse extends Response{
    public final List<NewsShortModel> payload;

    public NewsResponse(String resultCode, List<NewsShortModel> payload) {
        super(resultCode);
        this.payload = payload;
    }
}
