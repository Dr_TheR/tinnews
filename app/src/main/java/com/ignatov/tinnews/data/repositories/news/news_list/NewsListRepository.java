package com.ignatov.tinnews.data.repositories.news.news_list;


import com.ignatov.tinnews.data.database.entities.NewsTitleEntity;
import com.ignatov.tinnews.data.database.tables.NewsTitleTable;
import com.ignatov.tinnews.data.network.TinkoffNewsService;
import com.ignatov.tinnews.data.network.models.NewsShortModel;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio.sqlite.queries.Query;

import java.util.Date;
import java.util.List;

import rx.Observable;

public class NewsListRepository implements INewsListRepository {
    private StorIOSQLite database;
    private TinkoffNewsService network;

    public NewsListRepository(StorIOSQLite database,
                              TinkoffNewsService network) {
        this.database = database;
        this.network = network;
    }

    @Override
    public Observable<PutResults<NewsTitleEntity>> saveNewsList(List<News> newsList) {
        return Observable
                .from(newsList)
                .map(this::convertToDatabase)
                .toList()
                .flatMap(newsTitleEntities ->
                        database
                                .put()
                                .objects(newsTitleEntities)
                                .prepare()
                                .asRxObservable())
                .onErrorReturn((exception) -> null);
    }

    @Override
    public Observable<List<News>> getSavedNewsList() {
        return database
                .get()
                .listOfObjects(NewsTitleEntity.class)
                .withQuery(Query.builder()
                        .table(NewsTitleTable.TABLE)
                        .build())
                .prepare()
                .asRxSingle()
                .toObservable()
                .flatMap(Observable::from)
                .map(this::convertFromDatabase)
                .toList();
    }

    public Observable<List<News>> getNewsList() {
        return network
                .getNewsList()
                .map(newsResponse -> newsResponse.payload)
                .flatMap(Observable::from)
                .map(this::convertFromNetwork)
                .toList();
    }

    private NewsTitleEntity convertToDatabase(News news) {
        NewsTitleEntity result = new NewsTitleEntity();
        result.id = news.getNewsId();
        result.name = news.getTitle().name;
        result.publicationDate = news.getTitle().publicationDate.getTime();
        result.text = news.getTitle().text;
        return result;
    }

    private News convertFromDatabase(NewsTitleEntity title) {
        return new News(
                title.id,
                title.name,
                title.text,
                new Date(title.publicationDate)
        );
    }

    private News convertFromNetwork(NewsShortModel title) {
        return new News(
                title.id,
                title.name,
                title.text,
                title.publicationDate
        );
    }
}
