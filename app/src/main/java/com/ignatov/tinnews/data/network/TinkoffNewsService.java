package com.ignatov.tinnews.data.network;


import com.ignatov.tinnews.data.network.models.NewsContentResponse;
import com.ignatov.tinnews.data.network.models.NewsResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface TinkoffNewsService {
    @GET("news")
    Observable<NewsResponse> getNewsList();

    @GET("news_content")
    Observable<NewsContentResponse> getNewsContent(@Query("id") long id);
}
