package com.ignatov.tinnews.data.network.models;


import java.util.Date;

public class NewsShortModel {
    public long id;
    public String name;
    public String text;
    public Date publicationDate;
    public int bankInfoTypeId;
}
