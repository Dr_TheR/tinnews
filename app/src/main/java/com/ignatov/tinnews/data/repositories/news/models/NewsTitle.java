package com.ignatov.tinnews.data.repositories.news.models;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class NewsTitle implements Parcelable {
    public final String name;
    public final String text;
    public final Date publicationDate;


    NewsTitle(String name,
              String text,
              Date publicationDate) {
        this.name = name;
        this.text = text;
        this.publicationDate = publicationDate;
    }

    protected NewsTitle(Parcel in) {
        name = in.readString();
        text = in.readString();
        publicationDate = new Date(in.readLong());
    }

    //region Parcelable implementation
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(text);
        dest.writeLong(publicationDate.getTime());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NewsTitle> CREATOR = new Creator<NewsTitle>() {
        @Override
        public NewsTitle createFromParcel(Parcel in) {
            return new NewsTitle(in);
        }

        @Override
        public NewsTitle[] newArray(int size) {
            return new NewsTitle[size];
        }
    };
    //endregion


    //region hashCode and equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsTitle newsTitle = (NewsTitle) o;

        if (name != null ? !name.equals(newsTitle.name) : newsTitle.name != null) return false;
        if (text != null ? !text.equals(newsTitle.text) : newsTitle.text != null) return false;
        return publicationDate != null ? publicationDate.equals(newsTitle.publicationDate) : newsTitle.publicationDate == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
        return result;
    }
    //endregion
}
