package com.ignatov.tinnews.data.database.entities;

import android.support.annotation.NonNull;

import com.ignatov.tinnews.data.database.tables.NewsContentTable;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

@SuppressWarnings("NullableProblems")
@StorIOSQLiteType(table = NewsContentTable.TABLE)
public class NewsContentEntity {
    @NonNull
    @StorIOSQLiteColumn(name = NewsContentTable.COLUMN_ID, key = true)
    public Long id;

    @NonNull
    @StorIOSQLiteColumn(name = NewsContentTable.COLUMN_CREATION_DATE)
    public Long creationDate;

    @NonNull
    @StorIOSQLiteColumn(name = NewsContentTable.COLUMN_LAST_MODIFICATION_DATE)
    public Long lastModificationDate;

    @NonNull
    @StorIOSQLiteColumn(name = NewsContentTable.COLUMN_CONTENT)
    public String content;

    public NewsContentEntity() {
    }
}
