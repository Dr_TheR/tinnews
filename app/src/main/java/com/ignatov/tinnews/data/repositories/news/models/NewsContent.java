package com.ignatov.tinnews.data.repositories.news.models;


import java.util.Date;

public class NewsContent {
    public final Date creationDate;
    public final Date lastModificationDate;
    public final String content;

    public NewsContent(Date creationDate,
                       Date lastModificationDate,
                       String content) {
        this.creationDate = creationDate;
        this.lastModificationDate = lastModificationDate;
        this.content = content;
    }

    //region hashCode and equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsContent that = (NewsContent) o;

        if (creationDate != null ? !creationDate.equals(that.creationDate) : that.creationDate != null)
            return false;
        if (lastModificationDate != null ? !lastModificationDate.equals(that.lastModificationDate) : that.lastModificationDate != null)
            return false;
        return content != null ? content.equals(that.content) : that.content == null;

    }

    @Override
    public int hashCode() {
        int result = creationDate != null ? creationDate.hashCode() : 0;
        result = 31 * result + (lastModificationDate != null ? lastModificationDate.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
    //endregion
}
