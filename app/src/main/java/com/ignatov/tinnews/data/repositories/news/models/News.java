package com.ignatov.tinnews.data.repositories.news.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;

public class News implements Parcelable {
    private final long newsId;
    @NonNull
    private final NewsTitle title;
    @Nullable
    private NewsContent content;

    public News(long newsId,
                String name,
                String text,
                Date publicationDate) {
        this.newsId = newsId;
        title = new NewsTitle(
                name,
                text,
                publicationDate
        );
    }

    public long getNewsId() {
        return newsId;
    }

    @NonNull
    public NewsTitle getTitle() {
        return title;
    }

    @Nullable
    public NewsContent getContent() {
        return content;
    }

    public void setContent(@Nullable NewsContent content) {
        this.content = content;
    }

    //region Parcelable implementation
    protected News(Parcel in) {
        newsId = in.readLong();
        title = in.readParcelable(NewsTitle.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(newsId);
        dest.writeParcelable(title, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };
    //endregion

    //region hashCode and equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (newsId != news.newsId) return false;
        if (!title.equals(news.title)) return false;
        return content != null ? content.equals(news.content) : news.content == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + title.hashCode();
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
    //endregion
}
