package com.ignatov.tinnews.data.network.models;


public class NewsContentResponse extends Response{
    public final NewsFullModel payload;

    public NewsContentResponse(String resultCode, NewsFullModel payload) {
        super(resultCode);
        this.payload = payload;
    }
}
