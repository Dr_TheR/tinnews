package com.ignatov.tinnews.data.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ignatov.tinnews.dagger.application.DatabaseModule;
import com.ignatov.tinnews.data.database.tables.NewsContentTable;
import com.ignatov.tinnews.data.database.tables.NewsTitleTable;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "tinnews";
    private static final int DATABASE_VERSION = 7;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(DatabaseModule.TAG, "Database is creating.");
        db.execSQL(NewsTitleTable.getCreateTableQuery());
        db.execSQL(NewsContentTable.getCreateTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO Don't have time for normal implementing
        Log.i(DatabaseModule.TAG, "Database is updating. oldVersion=" + oldVersion + ", newVersion=" + newVersion);
        db.execSQL(NewsTitleTable.getDeleteTableQuery());
        db.execSQL(NewsContentTable.getDeleteTableQuery());
        onCreate(db);
    }
}
