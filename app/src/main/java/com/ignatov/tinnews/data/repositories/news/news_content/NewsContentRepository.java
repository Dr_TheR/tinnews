package com.ignatov.tinnews.data.repositories.news.news_content;


import com.ignatov.tinnews.data.database.entities.NewsContentEntity;
import com.ignatov.tinnews.data.database.tables.NewsContentTable;
import com.ignatov.tinnews.data.network.TinkoffNewsService;
import com.ignatov.tinnews.data.network.models.NewsFullModel;
import com.ignatov.tinnews.data.repositories.news.models.NewsContent;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.Query;

import java.util.Date;

import rx.Observable;
import rx.schedulers.Schedulers;

public class NewsContentRepository implements INewsContentRepository {
    private StorIOSQLite database;
    private TinkoffNewsService network;

    public NewsContentRepository(StorIOSQLite database,
                                 TinkoffNewsService network) {
        this.database = database;
        this.network = network;
    }

    /**
     * @return Observable will return NULL if it has error while implementing
     */
    @Override
    public Observable<PutResult> saveNewsContent(long id, NewsContent newsContent) {
        return database
                .put()
                .object(this.convertToDatabase(id, newsContent))
                .prepare()
                .asRxObservable()
                .onErrorReturn((exception) -> null)
                .subscribeOn(Schedulers.io());
    }

    /**
     * @return Observable will return EXCEPTION if it has error while implementing
     */
    @Override
    public Observable<NewsContent> getSavedNewsContent(final long newsId) {
        return database.get()
                .object(NewsContentEntity.class)
                .withQuery(Query.builder()
                        .table(NewsContentTable.TABLE)
                        .where(NewsContentTable.COLUMN_ID + " = ?")
                        .whereArgs(newsId)
                        .build())
                .prepare()
                .asRxObservable()
                .map(this::convertFromDatabase);
    }

    /**
     * @return Observable will return EXCEPTION if it has error while implementing
     */
    @Override
    public Observable<NewsContent> getNewsContent(final long newsId) {
        return network.getNewsContent(newsId)
                .map(newsContentResponse -> newsContentResponse.payload)
                .map(this::convertFromNetwork);
    }

    private NewsContentEntity convertToDatabase(long id, NewsContent newsContent) {
        NewsContentEntity result = new NewsContentEntity();
        result.id = id;
        result.content = newsContent.content;
        result.creationDate = newsContent.creationDate.getTime();
        result.lastModificationDate = newsContent.lastModificationDate.getTime();
        return result;
    }

    private NewsContent convertFromDatabase(NewsContentEntity newsContentEntity) {
        return new NewsContent(
                new Date(newsContentEntity.creationDate),
                new Date(newsContentEntity.lastModificationDate),
                newsContentEntity.content
        );
    }

    private NewsContent convertFromNetwork(NewsFullModel newsFullModel) {
        return new NewsContent(
                newsFullModel.creationDate,
                newsFullModel.lastModificationDate,
                newsFullModel.content
        );
    }
}
