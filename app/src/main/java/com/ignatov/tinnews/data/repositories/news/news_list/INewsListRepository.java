package com.ignatov.tinnews.data.repositories.news.news_list;


import com.ignatov.tinnews.data.database.entities.NewsTitleEntity;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;

import java.util.List;

import rx.Observable;

public interface INewsListRepository {
    /**
     * @return Observable will return EXCEPTION if it has error while implementing
     */
    Observable<List<News>> getSavedNewsList();
    /**
     * @return Observable will return EXCEPTION if it has error while implementing
     */
    Observable<List<News>> getNewsList();
    /**
     * @return Observable will return NULL if it has error while implementing
     */
    Observable<PutResults<NewsTitleEntity>> saveNewsList(List<News> newsList);
}
