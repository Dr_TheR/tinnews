package com.ignatov.tinnews.data.network.adapters;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

public class DateTypeAdapter implements JsonDeserializer<Date>{
    private static final String FIELD_MILLISECONDS = "milliseconds";

    @Override
    public Date deserialize(JsonElement json,
                            Type typeOfT,
                            JsonDeserializationContext context) throws JsonParseException {
        try {
            Long milliseconds = json.getAsJsonObject().get(FIELD_MILLISECONDS).getAsLong();
            return new Date(milliseconds);
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
