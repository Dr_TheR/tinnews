package com.ignatov.tinnews.data.repositories.news.news_content;


import com.ignatov.tinnews.data.repositories.news.models.NewsContent;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;

import rx.Observable;

public interface INewsContentRepository {
    Observable<NewsContent> getSavedNewsContent(long newsId);
    Observable<NewsContent> getNewsContent(long newsId);
    Observable<PutResult> saveNewsContent(long newsId, NewsContent content);
}
