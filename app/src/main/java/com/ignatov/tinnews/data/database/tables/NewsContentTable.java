package com.ignatov.tinnews.data.database.tables;


import android.support.annotation.NonNull;

public class NewsContentTable {
    public static final String TABLE = "news_content";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CREATION_DATE = "creation_date";
    public static final String COLUMN_LAST_MODIFICATION_DATE = "last_modification_date";
    public static final String COLUMN_CONTENT = "content";

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE
                + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + COLUMN_CREATION_DATE + " INTEGER NOT NULL, "
                + COLUMN_LAST_MODIFICATION_DATE + " INTEGER NOT NULL, "
                + COLUMN_CONTENT + " TEXT NOT NULL"
                + ");";
    }

    @NonNull
    public static String getDeleteTableQuery() {
        return "DROP TABLE IF EXISTS " + TABLE;
    }
}
