package com.ignatov.tinnews.business.news_content;


import android.util.Log;

import com.ignatov.tinnews.dagger.application.DatabaseModule;
import com.ignatov.tinnews.data.repositories.news.models.NewsContent;
import com.ignatov.tinnews.data.repositories.news.news_content.INewsContentRepository;
import com.ignatov.tinnews.utils.DateUtils;

import rx.Observable;

public class NewsContentInteractor implements INewsContentInteractor {
    private INewsContentRepository newsContentRepository;
    private DateUtils dateUtils;

    public NewsContentInteractor(INewsContentRepository newsContentRepository,
                                 DateUtils dateUtils) {
        this.newsContentRepository = newsContentRepository;
        this.dateUtils = dateUtils;
    }

    @Override
    public Observable<NewsContent> getNewsContentFromAllSources(final long id) {
        return Observable.mergeDelayError(
                newsContentRepository
                        .getSavedNewsContent(id),
                newsContentRepository
                        .getNewsContent(id)
                        .doOnNext(newsContent -> saveNewsContent(id, newsContent))
        );
    }

    private void saveNewsContent(long id, NewsContent content) {
        newsContentRepository
                .saveNewsContent(id, content)
                .subscribe(
                        newsPutResults -> {
                            //ignore
                        },
                        (error) -> {
                            Log.e(DatabaseModule.TAG, "Error while NewsContent was saving: " + error.getMessage());
                        }
                );
    }
}
