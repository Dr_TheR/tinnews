package com.ignatov.tinnews.business.news_content;


import com.ignatov.tinnews.data.repositories.news.models.NewsContent;

import rx.Observable;

public interface INewsContentInteractor {
    /**
     * @return Emit news from all sources so it can emit equal elements.
     * Observable will return EXCEPTION if it has error while implementing
     */
    Observable<NewsContent> getNewsContentFromAllSources(long id);
}
