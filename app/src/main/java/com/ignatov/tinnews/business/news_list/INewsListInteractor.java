package com.ignatov.tinnews.business.news_list;

import com.ignatov.tinnews.business.news_list.models.SortMethod;
import com.ignatov.tinnews.data.repositories.news.models.News;

import java.util.List;

import rx.Observable;

public interface INewsListInteractor {
    int ORDER_BY_PUBLICATION_DATE = 0;

    /**
     * @return Emit news from all sources so it can emit equal elements.
     * Observable will return EXCEPTION if it has error while implementing
     */
    Observable<List<News>> getNewsListFromAllSources();

    SortMethod<News> getSortMethod(int sortType);


}
