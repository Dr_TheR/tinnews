package com.ignatov.tinnews.business.news_list.models;

public interface SortMethod<T> {
    int compare(T item1, T item2);
    boolean areContentsTheSame(T oldItem, T newItem);
    boolean areItemsTheSame(T item1, T item2);
}
