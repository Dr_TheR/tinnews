package com.ignatov.tinnews.business.news_list;

import android.util.Log;

import com.ignatov.tinnews.business.news_list.models.SortMethod;
import com.ignatov.tinnews.dagger.application.DatabaseModule;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.data.repositories.news.news_list.INewsListRepository;
import com.ignatov.tinnews.utils.DateUtils;
import com.ignatov.tinnews.utils.TextUtils;

import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

public class NewsListInteractor implements INewsListInteractor {
    private INewsListRepository newsListRepository;
    private DateUtils dateUtils;
    private TextUtils textUtils;

    public NewsListInteractor(INewsListRepository newsListRepository,
                              DateUtils dateUtils,
                              TextUtils textUtils) {
        this.newsListRepository = newsListRepository;
        this.dateUtils = dateUtils;
        this.textUtils = textUtils;
    }

    private final SortMethod<News> ORDER_BY_PUBLICATION_DATE_INSTANCE = new SortMethod<News>() {
        @Override
        public int compare(News item1, News item2) {
            return dateUtils.compare(
                    item2.getTitle().publicationDate,
                    item1.getTitle().publicationDate
            );
        }

        @Override
        public boolean areContentsTheSame(News oldItem, News newItem) {
            return !(oldItem == null || newItem == null) && oldItem.equals(newItem);

        }

        @Override
        public boolean areItemsTheSame(News item1, News item2) {
            return item1.getNewsId() == item2.getNewsId();
        }
    };

    @Override
    public Observable<List<News>> getNewsListFromAllSources() {
        return Observable
                .mergeDelayError(
                        newsListRepository
                                .getSavedNewsList(),
                        newsListRepository
                                .getNewsList()
                                .doOnNext(this::saveNewsList)
                );
    }

    private void saveNewsList(List<News> newsList) {
        newsListRepository
                .saveNewsList(newsList)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        newsPutResults -> {
                            //ignore
                        },
                        error -> {
                            Log.e(DatabaseModule.TAG, "Error while NewsList was saving: " + error.getMessage());
                        }
                );
    }

    @SuppressWarnings("unchecked")
    @Override
    public SortMethod getSortMethod(int orderBy) {
        return ORDER_BY_PUBLICATION_DATE_INSTANCE;
    }
}
