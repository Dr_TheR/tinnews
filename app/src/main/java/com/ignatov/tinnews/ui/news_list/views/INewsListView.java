package com.ignatov.tinnews.ui.news_list.views;


import com.ignatov.tinnews.business.news_list.models.SortMethod;
import com.ignatov.tinnews.data.repositories.news.models.News;

import java.util.List;

import rx.Observable;

public interface INewsListView {
    void showNewsContent(News news);

    void setSortMethod(SortMethod<News> sortMethod);

    void showNewsList(List<News> newsList);

    void showLoading();

    void hideLoading();

    void showError(Throwable exception);

    Observable<News> newsSelectedEvent();
}
