package com.ignatov.tinnews.ui.news_content.views;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ignatov.tinnews.Application;
import com.ignatov.tinnews.R;
import com.ignatov.tinnews.dagger.news_content.NewsContentModule;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.ui.news_content.presenter.INewsContentPresenter;
import com.ignatov.tinnews.utils.TextUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

public class NewsContentFragment extends Fragment implements INewsContentView {
    private static String EXTRA_NEWS = "News";

    @Inject
    INewsContentPresenter newsContentPresenter;

    @Inject
    TextUtils textUtils;

    private TextView titleView;
    private TextView contentView;
    private TextView publicationDateView;
    private CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Application
                .get(getContext())
                .applicationComponent()
                .plus(new NewsContentModule())
                .inject(this);

        newsContentPresenter.setNews(getArguments().getParcelable(EXTRA_NEWS));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_content, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        collapsingToolbarLayout = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);

        titleView = (TextView) view.findViewById(R.id.title);
        contentView = (TextView) view.findViewById(R.id.content);
        publicationDateView = (TextView) view.findViewById(R.id.publication_date);
        contentView.setMovementMethod(LinkMovementMethod.getInstance());

        newsContentPresenter.bindView(this);

        AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener((appBarLayout1, verticalOffset) -> {
            float alpha = 1f - (float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange();
            publicationDateView.setAlpha(alpha);
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        newsContentPresenter.unbindView();
    }

    @Override
    public void showTitle(String title) {
        collapsingToolbarLayout.setTitle(textUtils.fromHtml(title));
        titleView.setText(textUtils.fromHtml(title));
    }

    @Override
    public void showContent(String content) {
        contentView.setText(textUtils.fromHtml(content));
    }

    @Override
    public void showPublicationDate(Date publicationDate) {
        String date = SimpleDateFormat
                .getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT)
                .format(publicationDate);
        collapsingToolbarLayout.setTitle(date);
        publicationDateView.setText(date);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(Throwable error) {
        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    public static NewsContentFragment newInstance(News news) {
        NewsContentFragment fragment = new NewsContentFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_NEWS, news);
        fragment.setArguments(bundle);

        return fragment;
    }
}
