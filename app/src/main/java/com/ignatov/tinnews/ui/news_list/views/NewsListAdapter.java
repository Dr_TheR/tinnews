package com.ignatov.tinnews.ui.news_list.views;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ignatov.tinnews.R;
import com.ignatov.tinnews.business.news_list.models.SortMethod;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.utils.TextUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.android.MainThreadSubscription;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> implements
        View.OnClickListener {
    @Nullable
    private SortMethod<News> sortMethod;
    private static final DateFormat DATE_FORMATTER = SimpleDateFormat.getDateTimeInstance(
            DateFormat.SHORT,
            DateFormat.SHORT
    );
    private TextUtils textUtils;

    @NonNull
    private final SortedList<News> newsList;

    private Set<OnNewsSelectedListener> onNewsSelectedListeners = new HashSet<>();

    public NewsListAdapter(TextUtils textUtils) {
        this.textUtils = textUtils;
        newsList = new SortedList<>(News.class, new SortedList.Callback<News>() {
            @Override
            public int compare(News o1, News o2) {
                if (sortMethod == null) {
                    return 0;
                }
                return sortMethod.compare(o1, o2);
            }

            @Override
            public boolean areContentsTheSame(News oldItem, News newItem) {
                return sortMethod != null && sortMethod.areContentsTheSame(oldItem, newItem);
            }

            @Override
            public boolean areItemsTheSame(News item1, News item2) {
                return sortMethod != null && sortMethod.areItemsTheSame(item1, item2);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }
        });
    }

    public void setSortMethod(@Nullable SortMethod<News> sortMethod) {
        newsList.beginBatchedUpdates();
        this.sortMethod = sortMethod;
        newsList.endBatchedUpdates();
    }

    public void putNewsList(List<News> newsList) {
        this.newsList.beginBatchedUpdates();
        for (News news : newsList) {
            this.newsList.add(news);
        }
        this.newsList.endBatchedUpdates();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.view_item_news_list, parent, false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        News news = newsList.get(position);
        holder.bind(news);
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public Observable<News> newsSelectedEvent() {
        return Observable.create(
                subscriber -> {
                    OnNewsSelectedListener listener = subscriber::onNext;
                    addOnNewsSelectedListener(listener);
                    subscriber.add(new MainThreadSubscription() {
                        @Override protected void onUnsubscribe() {
                            removeOnNewsSelectedListener(listener);
                        }
                    });
                }
        );
    }

    private void addOnNewsSelectedListener(OnNewsSelectedListener onNewsSelectedListener) {
        this.onNewsSelectedListeners.add(onNewsSelectedListener);
    }

    private void removeOnNewsSelectedListener(OnNewsSelectedListener onNewsSelectedListener) {
        this.onNewsSelectedListeners.remove(onNewsSelectedListener);
    }

    private void notifyOnNewsSelectedListener(News news) {
        for (OnNewsSelectedListener listener : onNewsSelectedListeners) {
            listener.onNewsSelected(news);
        }
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (tag instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) tag;
            notifyOnNewsSelectedListener(viewHolder.news);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleView;
        TextView publicationDateView;

        News news;

        public ViewHolder(View itemView) {
            super(itemView);
            titleView = (TextView) itemView.findViewById(R.id.title);
            publicationDateView = (TextView) itemView.findViewById(R.id.publication_date);
        }

        void bind(News news) {
            this.news = news;
            titleView.setText(textUtils.fromHtml(news.getTitle().text));
            publicationDateView.setText(DATE_FORMATTER.format(news.getTitle().publicationDate));
        }
    }

    public interface OnNewsSelectedListener {
        void onNewsSelected(News news);
    }
}
