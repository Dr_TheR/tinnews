package com.ignatov.tinnews.ui.news_list.views;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ignatov.tinnews.Application;
import com.ignatov.tinnews.R;
import com.ignatov.tinnews.business.news_list.models.SortMethod;
import com.ignatov.tinnews.dagger.news_list.NewsListModule;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.ui.news_content.views.NewsContentActivity;
import com.ignatov.tinnews.ui.news_list.presenter.INewsListPresenter;
import com.ignatov.tinnews.utils.TextUtils;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

public class NewsListFragment extends Fragment implements
        INewsListView, SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private NewsListAdapter newsListAdapter;

    @Inject
    INewsListPresenter newsListPresenter;

    @Inject
    TextUtils textUtils;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Application
                .get(getContext())
                .applicationComponent()
                .plus(new NewsListModule())
                .inject(this);

        newsListAdapter = new NewsListAdapter(textUtils);

        //TODO Implement Request Permission for database
        requestPermissions(new String[]{Manifest.permission_group.STORAGE}, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(toolbar);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);

        RecyclerView newsListView = (RecyclerView) view.findViewById(R.id.news_list);
        newsListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        newsListView.setAdapter(newsListAdapter);
        newsListPresenter.bindView(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        newsListPresenter.unbindView();
    }

    @Override
    public void onRefresh() {
        newsListPresenter.loadNews();
    }

    @Override
    public void showNewsContent(News news) {
        getActivity().startActivity(
                NewsContentActivity.createIntent(getActivity(), news)
        );
    }

    @Override
    public void setSortMethod(SortMethod<News> sortMethod) {
        newsListAdapter.setSortMethod(sortMethod);
    }

    @Override
    public void showNewsList(List<News> newsList) {
        newsListAdapter.putNewsList(newsList);
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(Throwable exception) {
        Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public Observable<News> newsSelectedEvent() {
        return newsListAdapter.newsSelectedEvent();
    }

    public static NewsListFragment newInstance() {
        return new NewsListFragment();
    }
}
