package com.ignatov.tinnews.ui.news_list.presenter;

import com.ignatov.tinnews.business.news_list.INewsListInteractor;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.ui.news_list.views.INewsListView;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class NewsListPresenter implements INewsListPresenter {
    private INewsListInteractor newsListInteractor;
    private NewsListPresenterCache newsListPresenterCache;

    private INewsListView newsListView;

    private CompositeSubscription subscriptions = new CompositeSubscription();

    public NewsListPresenter(INewsListInteractor newsListInteractor, NewsListPresenterCache newsListPresenterCache) {
        this.newsListInteractor = newsListInteractor;
        this.newsListPresenterCache = newsListPresenterCache;

        newsListPresenterCache.setSortMethod(
                newsListInteractor.getSortMethod(INewsListInteractor.ORDER_BY_PUBLICATION_DATE)
        );
    }

    @Override
    public void bindView(INewsListView view) {
        newsListView = view;
        newsListView.setSortMethod(newsListPresenterCache.getSortMethod());

        Subscription sub = newsListView
                .newsSelectedEvent()
                .subscribe(news -> newsListView.showNewsContent(news));
        subscriptions.add(sub);

        if (newsListPresenterCache.isLoadingComplete()) {
            newsListView.showNewsList(newsListPresenterCache.getNewsList());
        } else {
            loadNews();
        }
    }

    @Override
    public void unbindView() {
        subscriptions.clear();
        newsListView = null;
    }

    @Override
    public void clickToNews(long id) {
        newsListView.showNewsContent(newsListPresenterCache.getNewsById(id));
    }

    @Override
    public void loadNews() {
        Subscription sub = newsListInteractor
                .getNewsListFromAllSources()
                .onErrorReturn((error) -> new ArrayList<>())
                .doOnSubscribe(this::handleSubscribeLoadNews)
                .map(this::cacheAndFiltrateNewsList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::handleNextLoadNews,
                        this::handleErrorLoadNews,
                        this::handleCompleteLoadNews
                );
        subscriptions.add(sub);
    }

    private List<News> cacheAndFiltrateNewsList(List<News> newsList) {
        newsListPresenterCache.addNewsList(newsList);
        return newsListPresenterCache.getNewsList();
    }

    private void handleSubscribeLoadNews() {
        newsListPresenterCache.setLoadingComplete(false);
        newsListView.showLoading();

    }

    private void handleNextLoadNews(List<News> news) {
        newsListView.showNewsList(newsListPresenterCache.getNewsList());
    }

    private void handleErrorLoadNews(Throwable error) {
        newsListPresenterCache.setLoadingComplete(true);
        newsListView.hideLoading();
        newsListView.showError(error);
    }

    private void handleCompleteLoadNews() {
        newsListPresenterCache.setLoadingComplete(true);
        newsListView.hideLoading();
    }
}
