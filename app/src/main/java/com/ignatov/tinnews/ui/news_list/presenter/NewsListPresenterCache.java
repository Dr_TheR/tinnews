package com.ignatov.tinnews.ui.news_list.presenter;

import com.ignatov.tinnews.business.news_list.models.SortMethod;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NewsListPresenterCache {
    private Map<Long, News> newsMap = new ConcurrentHashMap<>();
    private boolean isLoadingComplete = false;
    private SortMethod<News> sortMethod;
    private DateUtils dateUtils;

    public NewsListPresenterCache(DateUtils dateUtils) {
        this.dateUtils = dateUtils;
    }

    public SortMethod<News> getSortMethod() {
        return sortMethod;
    }

    public void setSortMethod(SortMethod<News> sortMethod) {
        this.sortMethod = sortMethod;
    }

    public boolean isLoadingComplete() {
        return isLoadingComplete;
    }

    public void setLoadingComplete(boolean loadingComplete) {
        isLoadingComplete = loadingComplete;
    }

    public News getNewsById(long id) {
        return newsMap.get(id);
    }

    public List<News> getNewsList() {
        return new ArrayList<>(newsMap.values());
    }

    public void addNewsList(List<News> newsList) {
        for (News newNews : newsList) {
            News oldNews = newsMap.get(newNews.getNewsId());

            boolean needToReplace = true;
            if (oldNews != null) {
                needToReplace = dateUtils.compare(
                        newNews.getTitle().publicationDate,
                        oldNews.getTitle().publicationDate
                ) > 0;
            }

            if (needToReplace) {
                newsMap.put(newNews.getNewsId(), newNews);
            }
        }
    }
}
