package com.ignatov.tinnews.ui.news_content.presenter;


import android.support.annotation.CheckResult;

import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.data.repositories.news.models.NewsContent;
import com.ignatov.tinnews.utils.DateUtils;

public class NewsContentPresenterCache {
    private boolean isLoadingComplete = false;

    private DateUtils dateUtils;

    private News news;

    public NewsContentPresenterCache(DateUtils dateUtils) {
        this.dateUtils = dateUtils;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    @CheckResult
    public boolean setNewsContent(NewsContent content) {
        if (content == null) {
            return false;
        }

        NewsContent oldContent = news.getContent();

        boolean needToUpdate = true;
        if (oldContent != null) {
            needToUpdate = dateUtils.compare(
                    content.lastModificationDate,
                    oldContent.lastModificationDate
            ) > 0;
        }

        if (needToUpdate) {
            news.setContent(content);
        }

        return needToUpdate;
    }

    public boolean isLoadingComplete() {
        return isLoadingComplete;
    }

    public void setLoadingComplete(boolean loadingComplete) {
        isLoadingComplete = loadingComplete;
    }
}
