package com.ignatov.tinnews.ui.news_list.presenter;


import com.ignatov.tinnews.ui.news_list.views.INewsListView;

public interface INewsListPresenter {
    void bindView(INewsListView view);
    void unbindView();

    void clickToNews(long id);
    void loadNews();
}
