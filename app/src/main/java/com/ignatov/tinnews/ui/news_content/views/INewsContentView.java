package com.ignatov.tinnews.ui.news_content.views;

import java.util.Date;

public interface INewsContentView {
    void showTitle(String title);
    void showContent(String content);
    void showPublicationDate(Date publicationDate);

    void showLoading();
    void hideLoading();

    void showError(Throwable error);
}
