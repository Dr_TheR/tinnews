package com.ignatov.tinnews.ui.news_content.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.ignatov.tinnews.R;
import com.ignatov.tinnews.data.repositories.news.models.News;

public class NewsContentActivity extends AppCompatActivity {
    private static final String EXTRA_NEWS = "news";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_content);
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            News news = getIntent().getParcelableExtra(EXTRA_NEWS);
            transaction.replace(R.id.container, NewsContentFragment.newInstance(news));
            transaction.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static Intent createIntent(Context context, News news) {
        Intent intent = new Intent(context, NewsContentActivity.class);
        intent.putExtra(EXTRA_NEWS, news);

        return intent;
    }
}
