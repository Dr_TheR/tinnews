package com.ignatov.tinnews.ui.news_content.presenter;

import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.ui.news_content.views.INewsContentView;

public interface INewsContentPresenter {
    void bindView(INewsContentView view);
    void unbindView();

    void setNews(News news);
}
