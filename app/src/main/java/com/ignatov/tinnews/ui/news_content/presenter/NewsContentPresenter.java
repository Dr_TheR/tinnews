package com.ignatov.tinnews.ui.news_content.presenter;


import com.ignatov.tinnews.business.news_content.INewsContentInteractor;
import com.ignatov.tinnews.data.repositories.news.models.News;
import com.ignatov.tinnews.data.repositories.news.models.NewsContent;
import com.ignatov.tinnews.ui.news_content.views.INewsContentView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class NewsContentPresenter implements INewsContentPresenter {
    private INewsContentView newsContentView;
    private NewsContentPresenterCache newsContentPresenterCache;
    private INewsContentInteractor newsContentInteractor;
    private CompositeSubscription subscriptions = new CompositeSubscription();

    public NewsContentPresenter(INewsContentInteractor newsContentInteractor, NewsContentPresenterCache newsContentPresenterCache) {
        this.newsContentInteractor = newsContentInteractor;
        this.newsContentPresenterCache = newsContentPresenterCache;
    }

    @Override
    public void bindView(INewsContentView view) {
        this.newsContentView = view;
        if (newsContentPresenterCache.getNews() == null) {
            throw new IllegalStateException("Field 'News' didn't fill. Need to fill this field before bindView called");
        }
        showNews(newsContentPresenterCache.getNews());
        if (!newsContentPresenterCache.isLoadingComplete()) {
            loadNewsContent(newsContentPresenterCache.getNews().getNewsId());
        }
    }

    @Override
    public void unbindView() {
        subscriptions.clear();
        this.newsContentView = null;
    }

    @Override
    public void setNews(News news) {
        newsContentPresenterCache.setNews(news);
    }

    private void showNews(News news) {
        NewsContent content = news.getContent();
        if (content != null) {
            newsContentView.showContent(content.content);
        }
        newsContentView.showTitle(news.getTitle().text);
        newsContentView.showPublicationDate(news.getTitle().publicationDate);
    }

    private void loadNewsContent(long id) {
        Subscription sub = newsContentInteractor
                .getNewsContentFromAllSources(id)
                .onErrorReturn((error) -> null)
                .doOnSubscribe(this::handleSubscribeLoadNewsContent)
                .filter(this::cacheAndFiltrateNewsContent)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(newsContent -> newsContentPresenterCache.getNews())
                .subscribe(
                        this::handleNextLoadNewsContent,
                        this::handleErrorLoadNewsContent,
                        this::handleCompleteLoadNewsContent
                );
        subscriptions.add(sub);
    }

    private boolean cacheAndFiltrateNewsContent(NewsContent newsContent) {
        return newsContentPresenterCache.setNewsContent(newsContent);
    }

    private void handleSubscribeLoadNewsContent() {
        newsContentPresenterCache.setLoadingComplete(false);
        newsContentView.showLoading();
    }

    private void handleNextLoadNewsContent(News news) {
        showNews(news);
    }

    private void handleErrorLoadNewsContent(Throwable error) {
        newsContentPresenterCache.setLoadingComplete(true);
        newsContentView.hideLoading();
        newsContentView.showError(error);
    }

    private void handleCompleteLoadNewsContent() {
        newsContentPresenterCache.setLoadingComplete(true);
        newsContentView.hideLoading();
    }
}
