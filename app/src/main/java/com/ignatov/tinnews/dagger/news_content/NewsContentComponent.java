package com.ignatov.tinnews.dagger.news_content;


import com.ignatov.tinnews.ui.news_content.views.NewsContentFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {NewsContentModule.class})
@NewsContentScope
public interface NewsContentComponent {
    void inject(NewsContentFragment newsContentFragment);
}
