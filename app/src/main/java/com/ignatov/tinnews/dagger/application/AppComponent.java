package com.ignatov.tinnews.dagger.application;


import com.ignatov.tinnews.dagger.news_content.NewsContentComponent;
import com.ignatov.tinnews.dagger.news_content.NewsContentModule;
import com.ignatov.tinnews.dagger.news_list.NewsListComponent;
import com.ignatov.tinnews.dagger.news_list.NewsListModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, NetworkModule.class, DatabaseModule.class, UtilsModule.class})
@Singleton
public interface AppComponent {
    NewsContentComponent plus(NewsContentModule newsContentModule);
    NewsListComponent plus(NewsListModule newsListModule);
}
