package com.ignatov.tinnews.dagger.application;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.ignatov.tinnews.data.database.DatabaseHelper;
import com.ignatov.tinnews.data.database.entities.NewsContentEntity;
import com.ignatov.tinnews.data.database.entities.NewsContentEntitySQLiteTypeMapping;
import com.ignatov.tinnews.data.database.entities.NewsTitleEntity;
import com.ignatov.tinnews.data.database.entities.NewsTitleEntitySQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {
    public static final String TAG = "Database";

    @Provides
    @NonNull
    @Singleton
    public StorIOSQLite provideStorIOSQLite(@NonNull SQLiteOpenHelper helper) {
        return DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(helper)
                .addTypeMapping(NewsTitleEntity.class, new NewsTitleEntitySQLiteTypeMapping())
                .addTypeMapping(NewsContentEntity.class, new NewsContentEntitySQLiteTypeMapping())
                .build();
    }

    @Provides
    @Singleton
    @NonNull
    SQLiteOpenHelper provideSQLiteOpenHelper(@NonNull Context context) {
        return new DatabaseHelper(context);
    }
}
