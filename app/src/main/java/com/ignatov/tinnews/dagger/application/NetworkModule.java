package com.ignatov.tinnews.dagger.application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.ignatov.tinnews.data.network.TinkoffNewsService;
import com.ignatov.tinnews.data.network.adapters.DateTypeAdapter;

import java.util.Date;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    private static final String TINKOFF_NEWS_API_URL = "https://api.tinkoff.ru/v1/";

    @Provides
    @Singleton
    TinkoffNewsService provideTinkoffNewsService(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(TINKOFF_NEWS_API_URL)
                .build();

        return retrofit.create(TinkoffNewsService.class);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateTypeAdapter())
                .create();
    }
}
