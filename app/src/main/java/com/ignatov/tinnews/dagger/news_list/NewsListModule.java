package com.ignatov.tinnews.dagger.news_list;


import com.ignatov.tinnews.business.news_list.INewsListInteractor;
import com.ignatov.tinnews.business.news_list.NewsListInteractor;
import com.ignatov.tinnews.data.network.TinkoffNewsService;
import com.ignatov.tinnews.data.repositories.news.news_list.INewsListRepository;
import com.ignatov.tinnews.data.repositories.news.news_list.NewsListRepository;
import com.ignatov.tinnews.ui.news_list.presenter.INewsListPresenter;
import com.ignatov.tinnews.ui.news_list.presenter.NewsListPresenter;
import com.ignatov.tinnews.ui.news_list.presenter.NewsListPresenterCache;
import com.ignatov.tinnews.utils.DateUtils;
import com.ignatov.tinnews.utils.TextUtils;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsListModule {
    @Provides
    @NewsListScope
    INewsListInteractor provideINewsListInteractor(INewsListRepository newsListRepository,
                                                   DateUtils dateUtils,
                                                   TextUtils textUtils) {
        return new NewsListInteractor(newsListRepository, dateUtils, textUtils);
    }

    @Provides
    @NewsListScope
    INewsListRepository provideINewsListRepository(StorIOSQLite storIOSQLite,
                                                   TinkoffNewsService tinkoffNewsService) {
        return new NewsListRepository(storIOSQLite, tinkoffNewsService);
    }

    @Provides
    @NewsListScope
    NewsListPresenterCache provideNewsListPresenterCache(DateUtils dateUtils) {
        return new NewsListPresenterCache(dateUtils);
    }

    @Provides
    @NewsListScope
    INewsListPresenter provideINewsListPresenter(INewsListInteractor newsListInteractor,
                                                 NewsListPresenterCache newsListPresenterCache) {
        return new NewsListPresenter(newsListInteractor, newsListPresenterCache);
    }
}
