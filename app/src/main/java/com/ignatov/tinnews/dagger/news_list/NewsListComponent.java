package com.ignatov.tinnews.dagger.news_list;


import com.ignatov.tinnews.ui.news_list.views.NewsListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {NewsListModule.class})
@NewsListScope
public interface NewsListComponent {
    void inject(NewsListFragment newsListFragment);
}
