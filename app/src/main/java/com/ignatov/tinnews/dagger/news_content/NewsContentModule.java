package com.ignatov.tinnews.dagger.news_content;


import com.ignatov.tinnews.business.news_content.INewsContentInteractor;
import com.ignatov.tinnews.business.news_content.NewsContentInteractor;
import com.ignatov.tinnews.data.network.TinkoffNewsService;
import com.ignatov.tinnews.data.repositories.news.news_content.INewsContentRepository;
import com.ignatov.tinnews.data.repositories.news.news_content.NewsContentRepository;
import com.ignatov.tinnews.ui.news_content.presenter.INewsContentPresenter;
import com.ignatov.tinnews.ui.news_content.presenter.NewsContentPresenter;
import com.ignatov.tinnews.ui.news_content.presenter.NewsContentPresenterCache;
import com.ignatov.tinnews.utils.DateUtils;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsContentModule {
    @Provides
    @NewsContentScope
    INewsContentInteractor provideINewsContentInteractor(INewsContentRepository newsContentRepository,
                                                         DateUtils dateUtils) {
        return new NewsContentInteractor(newsContentRepository, dateUtils);
    }

    @Provides
    @NewsContentScope
    INewsContentRepository provideINewsContentRepository(StorIOSQLite storIOSQLite,
                                                         TinkoffNewsService tinkoffNewsService) {
        return new NewsContentRepository(storIOSQLite, tinkoffNewsService);
    }

    @Provides
    @NewsContentScope
    INewsContentPresenter provideINewsContentPresenter(INewsContentInteractor interactor,
                                                       NewsContentPresenterCache cache) {
        return new NewsContentPresenter(interactor, cache);
    }

    @Provides
    @NewsContentScope
    NewsContentPresenterCache provideNewsContentPresenterCache(DateUtils dateUtils) {
        return new NewsContentPresenterCache(dateUtils);
    }


}
