package com.ignatov.tinnews.dagger.application;

import com.ignatov.tinnews.utils.DateUtils;
import com.ignatov.tinnews.utils.TextUtils;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {
    @Provides
    @Singleton
    DateUtils provideDateUtils() {
        return new DateUtils();
    }

    @Provides
    @Singleton
    TextUtils provideTextUtils() {
        return new TextUtils();
    }
}
